class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to main_app.root_path, alert: 'У вас нет прав для просмотра этой страницы.'
  end

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  protected

  def configure_permitted_parameters
    attrs = [:nickname, :email, :password, :password_confirmation, :remember_me, stream_attributes: {}]

    devise_parameter_sanitizer.permit :sign_up, keys: attrs
    devise_parameter_sanitizer.permit :account_update, keys: attrs
  end

  private

  def record_not_found
    render plain: 'Not found!'
  end
end
