class ArticlesController < ApplicationController
  def index
    unless params[:sort_by]
      @articles = Article.all
    else
      @articles = Article.sorted_by_likes
    end
  end

  def show
    @article = Article.friendly.find(params[:id])
  end
end
