class ProfilesController < ApplicationController
  def show
    @profile = User.find_by_slug!(params[:slug])
  end
end
