class TournamentsController < ApplicationController
  before_action :authenticate_user!, only: [:register_player]

  def index
    @tournaments = Tournament.all
  end

  def show
    @tournament = Tournament.friendly.find(params[:id])
  end

  def register_player
    @tournament = Tournament.friendly.find(params[:tournament_id])

    if @tournament.players.count >= (@tournament.max_players || 9999)
      redirect_to @tournament, alert: 'Достигнуто максимальное количество участников!'
    elsif @tournament.players.include?(current_user)
      redirect_to dashboard_tournament_path(@tournament), alert: 'Вы уже зарегистрировались в турнире!'
    else
      @tournament.players << current_user
      @tournament.save

      redirect_to dashboard_tournament_path(@tournament), notice: 'Вы успешно зарегистрировались в турнире!'
    end
  end
end
