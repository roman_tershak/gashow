class DislikesController < ApplicationController
  def create
    if current_user.nil? || Dislike.where(dislikeable_id: params[:dislikeable_id], dislikeable_type: params[:dislikeable_type], user: current_user).any?
      render json: 'error', status: :unprocessable_entity
    else
      dislike = Dislike.create!(dislikeable_id: params[:dislikeable_id], dislikeable_type: params[:dislikeable_type], user: current_user)
      render json: dislike.to_json, status: :ok
    end
  end
end
