class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = Article.find(params[:article_id]).comments.new(user_id: current_user.id, body: params[:body])

    respond_to do |format|
      if @comment.save
        format.js
      end
    end
  end
end