class PagesController < ApplicationController
  def index
    @tournament = Tournament.last
    @streams = Stream.online
    @articles = Article.last(2)
  end

  def contacts
  end
end
