class LikesController < ApplicationController
  def create
    if current_user.nil? || Like.where(likeable_id: params[:likeable_id], likeable_type: params[:likeable_type], user: current_user).any?
      render json: 'error', status: :unprocessable_entity
    else
      like = Like.create!(likeable_id: params[:likeable_id], likeable_type: params[:likeable_type], user: current_user)
      render json: like.to_json, status: :ok
    end
  end
end
