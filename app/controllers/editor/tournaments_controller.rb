module Editor
  class TournamentsController < ApplicationController
    before_action :admin_user!
    before_action :set_tournament, except: [:index]

    def index
      @tournaments = Tournament.all
    end

    def edit
    end

    def update
      if @tournament.update_attributes(tournament_params)
        redirect_to editor_tournaments_path, notice: 'Tournament rules was updated!'
      else
        render :edit
      end
    end

    private

    def admin_user!
      return if current_user && current_user.admin?

      raise CanCan::AccessDenied
    end

    def set_tournament
      @tournament = Tournament.friendly.find(params[:id])
    end

    def tournament_params
      params.require(:tournament).permit(
        :rules
      )
    end
  end
end
