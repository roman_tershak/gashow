module Editor
  class ArticlesController < ApplicationController
    before_action :admin_user!
    before_action :set_article, only: [:edit, :update, :destroy]

    def index
      @articles = Article.all
    end

    def new
      @article = Article.new
    end

    def create
      @article = Article.new(article_params)

      if @article.save
        redirect_to editor_articles_path, notice: 'Article was successfully created!'
      else
        render :new
      end
    end

    def edit
    end

    def update
      if @article.update_attributes(article_params)
        redirect_to editor_articles_path, notice: 'Article was successfully updated!'
      else
        render :edit
      end
    end

    def destroy
      @article.destroy

      redirect_to editor_articles_path, notice: 'Article was successfully deleted!'
    end

    private

    def admin_user!
      return if current_user && current_user.admin?

      raise CanCan::AccessDenied
    end

    def set_article
      @article = Article.friendly.find(params[:id])
    end

    def article_params
      params.require(:article).permit(
        :title, :card_image, :body
      )
    end
  end
end
