module Dashboard
  class TournamentsController < ApplicationController
    before_action :authenticate_user!

    def index
      @tournaments = current_user.tournaments
    end

    def show
      @tournament = current_user.tournaments.friendly.find(params[:id])
    end
  end
end
