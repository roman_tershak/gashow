class Stream < ApplicationRecord
  belongs_to :user

  enum status: [:draft, :published]

  scope :present, -> { where.not(link: nil).where.not(link: '') }
  scope :online, -> { present.where(status: :published) }
end
