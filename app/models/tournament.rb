class Tournament < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_and_belongs_to_many :players, class_name: 'User'

  mount_uploader :avatar, AvatarUploader
end
