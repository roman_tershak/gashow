class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :nickname, uniqueness: true

  enum role: [:player, :admin]

  has_one :stream
  has_many :comments
  has_many :likes
  has_many :dislikes
  has_and_belongs_to_many :tournaments

  accepts_nested_attributes_for :stream

  def admin
    self.admin?
  end

  def name
    self.nickname
  end

  class << self
    def find_by_slug!(slug)
      User.find_by!(nickname: slug)
    end
  end
end
