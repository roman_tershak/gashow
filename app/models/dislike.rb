class Dislike < ApplicationRecord
  belongs_to :dislikeable, polymorphic: true
  belongs_to :user
end
