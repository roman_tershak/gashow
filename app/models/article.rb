class Article < ApplicationRecord
  default_scope { order('id DESC') }
  
  scope :sorted_by_likes, -> { unscoped.all.includes(:comments).sort {|a, b| b.rating <=> a.rating } }

  extend FriendlyId
  friendly_id :title, use: :slugged

  has_many :comments

  mount_uploader :card_image, AvatarUploader

  has_many :likes, as: :likeable
  has_many :dislikes, as: :dislikeable

  def rating
    self.likes.count - self.dislikes.count
  end
end
