class Comment < ApplicationRecord
  default_scope { order('id DESC') }
  
  belongs_to :article
  belongs_to :user

  has_many :likes, as: :likeable
  has_many :dislikes, as: :dislikeable
end
