module ApplicationHelper
  def dashboard_path?
    request.path =~ /dashboard/
  end

  def messages_path?
    request.path =~ /messages/ || request.path =~ /private-topics/
  end

  def forum_path?
    request.path =~ /forum/
  end

  def articles_path?
    request.path =~ /articles/
  end

  def contacts_path?
    request.path =~ /contacts/
  end

  def streams_path?
    request.path =~ /streams/
  end

  def tournaments_path?
    request.path =~ /tournaments/
  end

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
