(function () {

    'use strict';

    var modal;

    var attr = {

        modalID: {
            entry: 'entry',
            regUser: 'reg-user',
            regTourn: 'reg-tourn',
            agreement: 'agreement',
            restore: 'restore',
        },

        modalCall: {
            entry: '.modal-call--entry',
            regUser: '.modal-call--reg-user',
            regTourn: '.modal-call--reg-tourn',
            agreement: '.modal-call--agreement',

            entryHide: '.modal-call--entry-hide',
            regUserHide: '.modal-call--reg-user-hide',
            restorePassword: '.modal-form--restore',
        },

        menu: {
            list: '.menu--list'
        },

        scrollto: 'data-scroll-to',

        slide: 'data-slide-box',

    };

    var method = {

        resetData: function () {

            var proto = this.resetData.prototype = {

                init: function () {

                    var i = 0,
                        array = [],
                        form = arguments[0];

                    array.push(form.getElementsByTagName('input'));
                    array.push(form.getElementsByTagName('textarea'));

                    for (; i < array.length; i++) proto.action(array[i]);

                },

                action: function (nodes) {
                    var i = 0;
                    for (; i < nodes.length; i++) nodes[i].value = '';
                },

            };

            proto.init(arguments[0]);

        },

    };

    var fn = {

        tab: function () {

            __.tab({
                autoplay: false,
                effect: true,
                duration: 300,
                accordion: {
                    single: false,
                    opened: false,
                },
                display: 'block',
                parent: '.forum-body--item',
                button: '.forum-body--item--switch-arrow',
                container: '.forum-body--item--inner',
            });

        },

        modal: function () {

            modal = __.modal({
                effect: 'slide-left',
                duration: 500,
                scroll: true,
                close: {
                    inner: true,
                    outer: false,
                    space: true,
                },
                breakpoints: {
                    480: {
                        close: {
                            inner: true,
                            outer: false,
                            space: true,
                        },
                    },
                },
            });

        },

        swiper: {

            aside: function () {

                new Swiper('.swiper-slider--00', {
                    loop: true,
                    spaceBetween: 10,
                    navigation: {
                        prevEl: '.swiper-slider--00-prev',
                        nextEl: '.swiper-slider--00-next',
                    },
                });

                new Swiper('.swiper-slider--01', {
                    loop: true,
                    spaceBetween: 10,
                    navigation: {
                        prevEl: '.swiper-slider--01-prev',
                        nextEl: '.swiper-slider--01-next',
                    },
                });

            },

        },

        cut: function () {

            __('.slider--item-subtitle').cut(50);
            __('.newsbox--desc').cut(190);
            __('.news-grid--subtitle').cut(110);

        },

        click: {

            attr: {

                scrollto: '[' + attr.scrollto + ']',

                slide: '[' + attr.slide + ']',

                entry: attr.modalCall.entry,

                regUser: attr.modalCall.regUser,

                regTourn: attr.modalCall.regTourn,

                agreement: attr.modalCall.agreement,

                entryHide: attr.modalCall.entryHide,

                regUserHide: attr.modalCall.regUserHide,

                restorePassword: attr.modalCall.restorePassword,

            },

            method: {

                scrollto: function (event, target) {

                    event.preventDefault();

                    var id = target.getAttribute(attr.scrollto);

                    if (id) {

                        var offset = __('#' + id).offset();

                        if (offset) {

                            var headerHeight = __('.header--sticky').css('position') === 'fixed' ? __('.header--sticky').round('height') : 0;

                            console.log(headerHeight);

                            __.scroll().to({
                                duration: 300,
                                timing: 'linear',
                                offset: offset.top - headerHeight,
                            });

                        }

                    }

                },

                slide: function (event, target) {
                    
                    var container = __('#' + target.getAttribute(attr.slide));
                    
                    __(target).attr('data-slide-caption', __(container).css('display') === 'none' ? 'on' : 'off');
                    __(container).slide(__(container).css('display') === 'none' ? false : true, {
                        duration: 400,
                    });

                },

                entry: function (event, target) {
                    modal.show(attr.modalID.entry);
                },

                regUser: function (event, target) {
                    modal.show(attr.modalID.regUser);
                },

                regTourn: function (event, target) {
                    modal.show(attr.modalID.regTourn);
                },

                agreement: function (event, target) {
                    modal.show(attr.modalID.agreement);
                },

                entryHide: function (event, target) {
                    modal.hide(attr.modalID.regUser);
                    modal.delay.show(attr.modalID.entry, 500);
                },

                regUserHide: function (event, target) {
                    modal.hide(attr.modalID.entry);
                    modal.delay.show(attr.modalID.regUser, 500);
                },

                restorePassword: function (event, target) {
                    modal.hide(attr.modalID.entry);
                    modal.delay.show(attr.modalID.restore, 500);
                },

            },

            name: 'FrontEnd',

        },

        switch: function () {

            __('.comments-sort--item').switch();
            __('.forum-inner--head-follow').switch();

        },

        exceptTridend: function () {

            var insert = '<div class="browser-not-support--cover"><div class="browser-not-support--centered"><p class="browser-not-support--title">К сожалению Ваш браузер не поддерживает многих возможностей, которые предоставляет наш сервис.</p><dl><dt>Что Вы можете сделать:</dt><dd>Обновите браузер;</dd><dd>Попробуйте открыть страницу с помощью другого браузера;</dd><dd>Вы можете бесплатно скачать и пользоваться другими браузерами: <a href="https://www.google.com/chrome/?hl=ru" target="_blank">Google Chrome</a>, <a href="https://www.opera.com/ru" target="_blank">Opera</a>, <a href="https://www.mozilla.org/ru/firefox/new/" target="_blank">Firefox</a></dd></dl></div></div>';

            __(document.body).text('').css('position', 'fixed').insert('in', 'after', insert);

        },

        menu: function () {

            var buttonHide,
                container,
                buttonFixed;

            var attr = {
                hide: 'data-menu--hide',
                button: 'data-menu--switch',
                container: __.attr.animate,
                buttonFixed: 'data-menu--switch-fixed',
            };

            var className = {
                container: 'aside--cover',
                buttonFixed: 'menu--switch-fixed',
                buttonSpan: 'menu--switch-item',
            };

            var state = __.attr.state;

            var effect = __.effect.slideLeft;

            var proto = this.menu.prototype = {

                init: function () {
                    buttonHide = document.querySelector('[' + attr.hide + ']');
                    buttonFixed = document.getElementsByClassName(className.buttonFixed)[0];
                    if (!buttonHide && !buttonFixed) proto.started();
                },

                event: {

                    click: function (event) {

                        var target = event.target;

                        if (target.closest('[' + attr.button + ']')) {
                            var int = target.closest('[' + attr.button + ']').getAttribute(attr.button) === state[0] ? 1 : 0;
                            proto.switch(int);
                            return;
                        }

                        if (target.closest('[' + attr.hide + ']')) {
                            proto.switch(0);
                            return;
                        }

                    },

                    resize: function () {
                        proto.switch(__.viewport().width <= 1024 ? 0 : 1);
                    },

                },

                switch: function (pos) {
                    buttonHide.setAttribute(attr.hide, state[pos]);
                    buttonFixed.setAttribute(attr.button, state[pos]);
                    if (container) container.setAttribute(attr.container, effect[pos]);
                },

                started: function () {
                    buttonHide = proto.createHide();
                    buttonFixed = proto.createSwitch();
                    container = document.getElementsByClassName(className.container)[0];
                    container.setAttribute(attr.container, state[0]);
                    proto.event.resize();
                    document.addEventListener('click', proto.event.click);
                    window.addEventListener('resize', proto.event.resize);
                },

                createHide: function () {
                    buttonHide = document.createElement('div');
                    buttonHide.setAttribute(attr.hide, state[0]);
                    document.body.appendChild(buttonHide);
                    return buttonHide;
                },

                createSwitch: function () {

                    var i = 0;

                    buttonFixed = document.createElement('button');
                    buttonFixed.classList.add(className.buttonFixed);
                    buttonFixed.setAttribute(attr.button, state[0]);
                    buttonFixed.setAttribute(attr.buttonFixed, state[1]);

                    for (; i < 3; i++) {
                        var span = document.createElement('span');
                        span.classList.add(className.buttonSpan);
                        span.classList.add(className.buttonSpan + '-0' + i);
                        buttonFixed.appendChild(span);
                    }

                    document.body.appendChild(buttonFixed);
                    return buttonFixed;

                },

            };

            proto.init();

        },

        userOptions: function () {

            var button,
                container;

            var attr = {
                switch: 'data-autoriz--switch-button',
                animate: 'data-webcore--animate',
                button: 'autoriz-user--switch',
                container: 'autoriz-user--select',
            };

            var state = __.attr.state;

            var proto = this.userOptions.prototype = {

                init: function () {

                    button = document.getElementsByClassName(attr.button)[0];
                    container = document.getElementsByClassName(attr.container)[0];

                    if (button && container) {
                        button.setAttribute(attr.switch, state[0]);
                        container.setAttribute(attr.animate, state[0]);
                        document.addEventListener('click', proto.event);
                    }

                },

                event: function (event) {

                    var target = event.target;

                    if (target.closest('.' + attr.button)) {
                        
                        var item = target.closest('.' + attr.button),
                            count = item.getAttribute(attr.switch) === state[0] ? 1 : 0;
                        
                        item.setAttribute(attr.switch, state[count]);
                        container.setAttribute(attr.animate, state[count]);
                        
                        event.preventDefault();

                        return;

                    }

                    button.setAttribute(attr.switch, state[0]);
                    container.setAttribute(attr.animate, state[0]);

                },

            };

            proto.init();

        },

    };

    var ready = {

        complete: function () {},

        interacive: function () {

            if (__.browser().core !== 'trident') {

                fn.userOptions();
                fn.tab();
                fn.modal();
                fn.swiper.aside();
                fn.cut();
                fn.switch();
                fn.menu();

                __(attr.menu.list).sticky(140);
                __('.users-online--cover').sticky(0);
                
                __.click(fn.click);

                __.scroll().key({
                    duration: 500,
                });

                __.select({
                    node: '.forum-create--select',
                    unique: {
                        parent: '.forum-create--select-parent',
                        caption: '.forum-create--select-caption',
                    },
                });

                return;

            }

            fn.exceptTridend();

        },

    };

    __(ready);

})(window, document);
