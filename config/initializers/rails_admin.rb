RailsAdmin.config do |config|

  ### Popular gems integration

  # == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  # == Cancan ==
  config.authorize_with :cancancan

  config.parent_controller = 'ApplicationController'

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'User' do
    label 'Пользователи'

    list do
      field :nickname
      field :email
      field :role
    end
  end

  config.model 'Tournament' do
    label 'Турниры'

    list do
      field :name
      field :max_players
      field :end_registration_date
      field :prize_fund
      field :tournament_password, :string
    end

    create do
      include_all_fields
      configure :slug do
        hide
      end
      configure :rules do
        hide
      end
    end

    edit do
      include_all_fields
      configure :slug do
        hide
      end
      configure :rules do
        hide
      end
    end
  end

  config.model 'Stream' do
    label 'Стримы игроков'

    list do
      scopes [:present]

      field :user
      field :link
      field :status
    end
  end

  config.included_models = [User, Tournament, Stream]
end
