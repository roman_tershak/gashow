Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'pages#index'

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  resources :tournaments, only: [:index, :show] do
    put 'register_player'
  end
  resources :streams, only: [:index]
  resources :articles, only: [:index, :show]

  namespace :dashboard do
    resources :tournaments, only: [:index, :show]
  end

  namespace :editor do
    resources :articles
    resources :tournaments
  end

  resources :likes, only: [:create]
  resources :dislikes, only: [:create]
  resources :comments, only: [:create]

  mount Thredded::Engine => '/forum'

  get 'contacts', to: 'pages#contacts'
  get 'profile/:slug', to: 'profiles#show', as: :profile
end
