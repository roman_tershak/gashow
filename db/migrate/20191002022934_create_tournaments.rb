class CreateTournaments < ActiveRecord::Migration[5.2]
  def change
    create_table :tournaments do |t|
      t.string :name
      t.string :avatar
      t.integer :max_players
      t.integer :prize_fund
      t.datetime :end_registration_date
      t.string :tournament_password
      t.string :rules
    end

    create_table :tournaments_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :tournament, index: true
    end
  end
end
