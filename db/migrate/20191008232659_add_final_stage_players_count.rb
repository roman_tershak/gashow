class AddFinalStagePlayersCount < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :final_stage_players_count, :integer, default: 8
  end
end
