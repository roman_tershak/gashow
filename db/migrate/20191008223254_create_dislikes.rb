class CreateDislikes < ActiveRecord::Migration[5.2]
  def change
    create_table :dislikes do |t|
      t.integer :user_id
      t.integer :dislikeable_id
      t.string :dislikeable_type
    end
  end
end
