class CreateStreams < ActiveRecord::Migration[5.2]
  def change
    create_table :streams do |t|
      t.belongs_to :user, index: true
      t.string :link
      t.integer :status, default: 0
    end
  end
end
