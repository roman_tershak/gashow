### How to run Application on server:

1. Create a new Ruby application on [Heroku](https://www.heroku.com/)

2. Run following commands:

* `git remote add heroku https://git.heroku.com/gashow.git`

* `git push heroku master --force`

* `heroku open`

3. Add custom domain for `gashow.ru` and `www.gashow.ru` following this instruction: [instructions](https://medium.com/@ethanryan/setting-up-a-custom-domain-for-your-heroku-hosted-app-6c011e75aa3d/)
